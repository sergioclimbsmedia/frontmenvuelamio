import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GanadoresVideoPage } from './ganadores-video';

@NgModule({
  declarations: [
    GanadoresVideoPage,
  ],
  imports: [
    IonicPageModule.forChild(GanadoresVideoPage),
  ],
})
export class GanadoresVideoPageModule {}
