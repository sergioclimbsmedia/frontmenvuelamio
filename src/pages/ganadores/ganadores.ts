import { GanadoresQuizPage } from './../ganadores-quiz/ganadores-quiz';
import { GanadoresVideoPage } from './../ganadores-video/ganadores-video';
import { GanadoresFotosPage } from './../ganadores-fotos/ganadores-fotos';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GanadoresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ganadores',
  templateUrl: 'ganadores.html',
})
export class GanadoresPage {
  public  tituhead:string="Ganadores";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GanadoresPage');
  }
 
  
  ganaFoto(){
    this.navCtrl.push(GanadoresFotosPage);
  }
   ganaVideo(){
   this.navCtrl.push(GanadoresVideoPage);
   } 
   
   quiz(){
  this.navCtrl.push(GanadoresQuizPage);
   }

}
