import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the EvenEspecialesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-even-especiales',
  templateUrl: 'even-especiales.html',
})
export class EvenEspecialesPage {

  
    //input
  public  tituhead:string="Eventos Especiales";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EvenEspecialesPage');
  }
  participar(){
    alert("vas a participar");
  }

}
