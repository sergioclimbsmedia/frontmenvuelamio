import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EvenEspecialesPage } from './even-especiales';

@NgModule({
  declarations: [
    EvenEspecialesPage,
  ],
  imports: [
    IonicPageModule.forChild(EvenEspecialesPage),
  ],
})
export class EvenEspecialesPageModule {}
