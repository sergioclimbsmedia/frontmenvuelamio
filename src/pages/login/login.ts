import { TabsPage } from '../tabs/tabs';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistrarsePage } from '../registrarse/registrarse';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

    aHome(){
    this.navCtrl.push(TabsPage);
   }
   recordarPsw(){
     alert("recordaremos tu contrasenia");
   }

   aFace(){
  this.navCtrl.push("hhtp://www.google.com");
   }
   aGmai(){
  alert("ir a gmail");
   }
   registrarte(){
     this.navCtrl.push(RegistrarsePage);
   }

}
