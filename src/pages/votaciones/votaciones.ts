import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { VotaRetoFotoPage } from '../vota-reto-foto/vota-reto-foto';

/**
 * Generated class for the VotacionesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-votaciones',
  templateUrl: 'votaciones.html',
})
export class VotacionesPage {
 
  public  tituhead:string="Votaciones";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VotacionesPage');
  }

  votaFoto(){
    this.navCtrl.push(VotaRetoFotoPage);
  }
   votaVideo(){
     alert('vota reto video');
   //this.navCtrl.push(GanadoresVideoPage);
   } 
   
   quiz(){
     alert('quiz');
  //this.navCtrl.push(GanadoresQuizPage);
   }

}
