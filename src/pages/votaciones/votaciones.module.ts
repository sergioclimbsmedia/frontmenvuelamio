import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VotacionesPage } from './votaciones';

@NgModule({
  declarations: [
    VotacionesPage,
  ],
  imports: [
    IonicPageModule.forChild(VotacionesPage),
  ],
})
export class VotacionesPageModule {}
