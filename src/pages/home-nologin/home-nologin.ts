import { LoginPage } from './../login/login';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { RegistrarsePage } from '../registrarse/registrarse';
import { EvenEspecialesPage } from '../even-especiales/even-especiales';
import { GanadoresPage } from '../ganadores/ganadores';
import { EventosRetoPage } from '../eventos-reto/eventos-reto';

@IonicPage()
@Component({
  selector: 'page-home-nologin',
  templateUrl: 'home-nologin.html',
})
export class HomeNologinPage {

  accion:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private  alertCtrl:AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomeNologinPage');
  }

  aLogin(){
   this.navCtrl.push(LoginPage);
  }
  

  alerta(){
   const alert = this.alertCtrl.create({
    title: 'No puedes Avanzar',
    subTitle:'Registrate o Logeate',
    buttons: [
      {
        text: 'Registrate',
       
        handler: ()=> {
          let texto="registrate";
          this.accion=texto;
          this.navCtrl.push(RegistrarsePage)
        }
      },
      {
        text: 'Logeate',
       
        handler: ()=> {
         let texto="logeate";
         this.accion=texto;
         console.log(this.accion);
         this.navCtrl.push(LoginPage)
        }
      }
    ] //  fin de butons
   
  });
 
  alert.present();

 
}

evEspeciales(){

  this.navCtrl.push(EvenEspecialesPage);
 }

  ganadores(){
     this.navCtrl.push(GanadoresPage);
   }

 eventosReto(){

  this.navCtrl.push(EventosRetoPage);
 }



}// fin de la clase
