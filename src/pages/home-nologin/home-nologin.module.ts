import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomeNologinPage } from './home-nologin';

@NgModule({
  declarations: [
    HomeNologinPage,
  ],
  imports: [
    IonicPageModule.forChild(HomeNologinPage),
  ],
})
export class HomeNologinPageModule {}
