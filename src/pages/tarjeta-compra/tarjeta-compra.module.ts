import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TarjetaCompraPage } from './tarjeta-compra';

@NgModule({
  declarations: [
    TarjetaCompraPage,
  ],
  imports: [
    IonicPageModule.forChild(TarjetaCompraPage),
  ],
})
export class TarjetaCompraPageModule {}
