import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CabezeraPage } from './cabezera';

@NgModule({
  declarations: [
    CabezeraPage,
  ],
  imports: [
    IonicPageModule.forChild(CabezeraPage),
  ],
})
export class CabezeraPageModule {}
