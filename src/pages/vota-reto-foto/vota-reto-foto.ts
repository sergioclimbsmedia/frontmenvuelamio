import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,PopoverController} from 'ionic-angular';

/**
 * Generated class for the VotaRetoFotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-vota-reto-foto',
  templateUrl: 'vota-reto-foto.html',
})
export class VotaRetoFotoPage {

  //para cabezera
  public  tituhead:string="Retos de Fotografia Votaciones";
  //para contenido
  usuario:string;

  //para opover
  data:any={};
  //regresa desde popover
  votado:string;
  //mensaje de votado
   valorhiden:boolean;
  
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public popoverCtrl: PopoverController) {
                this.usuario='alejandro';
                this.valorhiden=true;
               }

  ionViewDidLoad() {
    this.usuario='alejandro';
 
    console.log('ionViewDidLoad VotaRetoFotoPage');
  }


  votaFo(nomUser){

    this.data = {
                 imagen: nomUser+'.jpg',
                 concursante:nomUser ,
                 descripcion:`Paisaje Espectacular, que describe mucha
                              naturaleza `};
     
     
        let popover = this.popoverCtrl.create(PopoverRetoFoto,this.data, {cssClass: 'popover-retofotografia'});
        popover.present({
         // ev: myEvent
        });
        popover.onDidDismiss(Data=>{
      
          if(Data!=null){
            console.log('*****'+Data);
            this.votado=Data;

            setTimeout(()=>{    
              this.valorhiden= false;
               }, 200);
               setTimeout(()=>{    
                this.valorhiden= true;
                 }, 5000);
          }
          
        });
   
  }

   votaVi(){
   
     alert('vota video a usuario');
   //this.navCtrl.push(GanadoresVideoPage);
   } 
   
   quiz(){
     alert('quiz');
  //this.navCtrl.push(GanadoresQuizPage);
   }

}// fin de la clase

@Component({
  templateUrl: 'popRetoFotografia.html'

})
export class PopoverRetoFoto{

  imag:any;
  concursante:string;
  srcImagen:string;
  descripcion:string;
  
  imagenUrl="./../../assets/imgs/concursante/";
  constructor(public viewCtrl: ViewController,public NavParams:NavParams) {
  
   this.imag=this.NavParams.get('imagen');
   this.imagenUrl+=this.imag;
   this.srcImagen=this.imagenUrl;
   this.concursante=this.NavParams.get('concursante');

   this.descripcion=this.NavParams.get('descripcion');
   // console.log(this.descripcion +"---"+this.srcImagen);
  }

  cierra(concursante?:string) {
    let concur=concursante;
   
    if(concur!=null){
      this.viewCtrl.dismiss(concursante);
      console.log("votaste");
    } else{
      this.viewCtrl.dismiss();
      console.log("cerraste");
    }
   
  }

  votar(concursante){
  alert("le diste tu voto a "+concursante);
  this.cierra(concursante);
  }
}

