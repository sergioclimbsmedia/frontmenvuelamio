import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VotaRetoFotoPage } from './vota-reto-foto';

@NgModule({
  declarations: [
    VotaRetoFotoPage,
  ],
  imports: [
    IonicPageModule.forChild(VotaRetoFotoPage),
  ],
})
export class VotaRetoFotoPageModule {}
