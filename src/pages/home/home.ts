import { GanadoresPage } from './../ganadores/ganadores';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { EvenEspecialesPage } from '../even-especiales/even-especiales';
import { VotacionesPage } from '../votaciones/votaciones';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  //input
  public  tituhead:string="Home";

  constructor(public navCtrl: NavController) {

  }

  eventRetro(){
  alert("retrocede imagen");
  }

  eventAvan(){
    alert("avanza imagen");

  }
  evEspeciales(){

  this.navCtrl.push(EvenEspecialesPage);
 }

  ganadores(){
     this.navCtrl.push(GanadoresPage);
   }

   
   votaciones(){

  this.navCtrl.push(VotacionesPage);
 }
  
 
}
