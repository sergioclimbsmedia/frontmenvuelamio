import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GanadoresFotosPage } from './ganadores-fotos';

@NgModule({
  declarations: [
    GanadoresFotosPage,
  ],
  imports: [
    IonicPageModule.forChild(GanadoresFotosPage),
  ],
})
export class GanadoresFotosPageModule {}
