import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventosRetoPage } from './eventos-reto';

@NgModule({
  declarations: [
    EventosRetoPage,
  ],
  imports: [
    IonicPageModule.forChild(EventosRetoPage),
  ],
})
export class EventosRetoPageModule {}
