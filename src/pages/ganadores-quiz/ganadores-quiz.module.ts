import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GanadoresQuizPage } from './ganadores-quiz';

@NgModule({
  declarations: [
    GanadoresQuizPage,
  ],
  imports: [
    IonicPageModule.forChild(GanadoresQuizPage),
  ],
})
export class GanadoresQuizPageModule {}
