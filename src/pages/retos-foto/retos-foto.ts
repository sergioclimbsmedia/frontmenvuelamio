import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RetosFotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-retos-foto',
  templateUrl: 'retos-foto.html',
})
export class RetosFotoPage {
  public  tituhead:string="Retos de Fotografia";
  usuario:string='alejandro';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RetosFotoPage');
  }
  participa(){
    alert("participa");
  }

}
