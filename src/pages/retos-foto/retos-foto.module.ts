import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RetosFotoPage } from './retos-foto';

@NgModule({
  declarations: [
    RetosFotoPage,
  ],
  imports: [
    IonicPageModule.forChild(RetosFotoPage),
  ],
})
export class RetosFotoPageModule {}
