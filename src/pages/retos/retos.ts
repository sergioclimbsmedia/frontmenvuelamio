import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RetosFotoPage } from '../retos-foto/retos-foto';
import { RetosVideoPage } from '../retos-video/retos-video';
import { RetosQuizPage } from '../retos-quiz/retos-quiz';

/**
 * Generated class for the RetosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-retos',
  templateUrl: 'retos.html',
})
export class RetosPage {
  public  tituhead:string="Retos";
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RetosPage');
  }
  
 fotoReto(){
  this.navCtrl.push(RetosFotoPage);
 }
 
videoReto(){
  alert("videoreto");
 // this.navCtrl.push(RetosVideoPage);
}
quizReto(){
  alert("quiz");
 // this.navCtrl.push(RetosQuizPage);
}

}
