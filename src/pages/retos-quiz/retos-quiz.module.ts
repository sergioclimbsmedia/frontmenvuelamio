import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RetosQuizPage } from './retos-quiz';

@NgModule({
  declarations: [
    RetosQuizPage,
  ],
  imports: [
    IonicPageModule.forChild(RetosQuizPage),
  ],
})
export class RetosQuizPageModule {}
