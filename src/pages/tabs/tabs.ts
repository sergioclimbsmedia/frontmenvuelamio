
import { Component } from '@angular/core';


import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { TarjetasPage } from '../tarjetas/tarjetas';
import { RetosPage } from '../retos/retos';

@Component({
  templateUrl: 'tabs.html'
})

export class TabsPage {

  tab1Root = HomePage;

  tab2Root = RetosPage;
  
  tab3Root = ContactPage;
  tab4Root =TarjetasPage;

  constructor() {

  }
}
