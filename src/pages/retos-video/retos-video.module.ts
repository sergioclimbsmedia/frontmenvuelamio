import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RetosVideoPage } from './retos-video';

@NgModule({
  declarations: [
    RetosVideoPage,
  ],
  imports: [
    IonicPageModule.forChild(RetosVideoPage),
  ],
})
export class RetosVideoPageModule {}
