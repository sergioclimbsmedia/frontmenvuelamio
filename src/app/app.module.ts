
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { CabezeraPage } from '../pages/cabezera/cabezera';
import { TabsPage } from '../pages/tabs/tabs';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TarjetasPage } from '../pages/tarjetas/tarjetas';
import { HomeNologinPage } from '../pages/home-nologin/home-nologin';
import { LoginPage } from '../pages/login/login';
import { RegistrarsePage } from '../pages/registrarse/registrarse';
import { GanadoresPage } from './../pages/ganadores/ganadores';
import { GanadoresQuizPage } from './../pages/ganadores-quiz/ganadores-quiz';
import { GanadoresVideoPage } from './../pages/ganadores-video/ganadores-video';
import { GanadoresFotosPage } from './../pages/ganadores-fotos/ganadores-fotos';
import { EventosRetoPage } from '../pages/eventos-reto/eventos-reto';
import { RetosPage } from '../pages/retos/retos';
import { RetosFotoPage } from '../pages/retos-foto/retos-foto';
import { RetosVideoPage } from '../pages/retos-video/retos-video';
import { RetosQuizPage } from '../pages/retos-quiz/retos-quiz';
import { TarjetaCompraPage } from '../pages/tarjeta-compra/tarjeta-compra';
import { EvenEspecialesPage } from '../pages/even-especiales/even-especiales';
import { VotacionesPage } from '../pages/votaciones/votaciones';
import { VotaRetoFotoPage, PopoverRetoFoto } from '../pages/vota-reto-foto/vota-reto-foto';





@NgModule({
  declarations: [
    MyApp,
    ContactPage,CabezeraPage,RetosPage,RetosFotoPage,RetosVideoPage,RetosQuizPage,
    HomePage,TabsPage,TarjetasPage,HomeNologinPage,
    LoginPage,RegistrarsePage,GanadoresPage,
    EvenEspecialesPage,
    GanadoresFotosPage,GanadoresVideoPage,GanadoresQuizPage,EventosRetoPage,
    TarjetaCompraPage,
    VotacionesPage,
    VotaRetoFotoPage,PopoverRetoFoto
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,CabezeraPage,
    ContactPage,RetosPage,RetosFotoPage,RetosVideoPage,RetosQuizPage,
    HomePage,TabsPage,TarjetasPage,HomeNologinPage,
    LoginPage,RegistrarsePage,GanadoresPage,
    EvenEspecialesPage,
    GanadoresFotosPage,GanadoresVideoPage,GanadoresQuizPage,
    EventosRetoPage,TarjetaCompraPage,
    VotacionesPage,
    VotaRetoFotoPage,PopoverRetoFoto
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
