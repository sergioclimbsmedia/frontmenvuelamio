
import { HomePage } from './../pages/home/home';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { HomeNologinPage } from './../pages/home-nologin/home-nologin';
import { CabezeraPage } from '../pages/cabezera/cabezera';
import { RegistrarsePage } from '../pages/registrarse/registrarse';
import { RetosFotoPage } from '../pages/retos-foto/retos-foto';

import { EvenEspecialesPage } from '../pages/even-especiales/even-especiales';

import { VotaRetoFotoPage } from '../pages/vota-reto-foto/vota-reto-foto';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

   rootPage:any =HomeNologinPage;

  //rootPage:any = TabsPage; // me lleva al login

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
